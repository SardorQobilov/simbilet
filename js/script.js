$(function () {

	$('input[name=phone]').mask('+7 (999) 999-99-99');


	$('.menu-opener').on('click', function (e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$('.navbar').toggleClass('active');
	});

	var swiper3 = new Swiper(".quality-slide-in", {
		slidesPerView: 1,
		spaceBetween: 10,
		loop: true,
		speed: 1000,
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
		},
		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev",
		},
		breakpoints: {
			640: {
				slidesPerView: 1,
			},
			768: {
				slidesPerView: 1,
			},
			1024: {
				slidesPerView: 1,
			},
		},
	});

	// concert sec
	try {
		$('.more-text').slideUp(0);
		$('.show-text').each(item => {
			const card = $('.show-text')[item];
			$(card).find('.more-text__btn').click(function () {
				$(card).find('.more-text').slideToggle(400);
				$(card).find('.dots').toggleClass('d-none');

				if ($(this).text() == "Читать далее") {
					$(this).text("Читать меньше");
				}
				else {
					$(this).text("Читать далее");
				}
			})
		})

		$('.reviews-card__box').slideUp(0);
		$(".show-more__card").click(function () {
			$('.reviews-card__box').slideToggle(400);
			if ($(this).text() == "СКРЫВАТЬ") {
				$(this).text("ПОКАЗАТЬ ЕЩЕ");
			} else {
				$(this).text("СКРЫВАТЬ");
			}
		})


		var concertSwiper = new Swiper(".concert-swiper", {
			slidesPerView: 2.4,
			grabCursor: true,
			spaceBetween: 30,
			clickable: true,
			loop: true,
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			breakpoints: {
				360: {
					slidesPerView: 1.1,
					spaceBetween: 15,
				},
				430: {
					slidesPerView: 2.2,
					spaceBetween: 20,
				},
				640: {
					slidesPerView: 2.2,
					spaceBetween: 20,
				},
				768: {
					slidesPerView: 2.4,
					spaceBetween: 30,
				},
			},
		});
	} catch (error) {
		console.log(erorr);
	}

});

